<?php
$template_directory = get_template_directory_uri() . "/img/";
?>
<div class="container">
    <div class="row fonte-azul-rodape">
        <div class="col s12 m12 padTop10">
            <div class="col s12 m1">
                <img class="logo-rodape" src="<?php echo $template_directory; ?>Logo.png">
            </div>
            <div class="col s12 m3 padTop5">
                Copyright ©️ AdotePet 2021
            </div>
            <div class="col m1">
                &nbsp;
            </div>
            <div class="col m5 padTop5 right-align">Desenvolvido com um carinho danado por:
            </div>
            <div class="col m2">
                <img class="logo-rodape" src="<?php echo $template_directory; ?>al.png">
            </div>
        </div>
    </div>
</div>

</body>

</html>