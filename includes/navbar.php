<?php
$template_directory = get_template_directory_uri() . "/img/";

// $URL_ATUAL= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$URL_ATUAL = "$_SERVER[REQUEST_URI]";
// $URL_ATUAL= "$_SERVER[PHP_SELF]";
// echo $URL_ATUAL;
?>

<nav>
    <div class="nav-wrapper container">
        <a href="<?php echo get_site_url(); ?>" class="brand-logo">
            <img class="logo" src="<?php echo $template_directory . 'logo.png' ?>" alt="">
        </a>
        <a href="" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li class="item-nav-li">
                <a class="<?php if($URL_ATUAL == '/adotepet/sobre-nos/'){echo 'nav-active';};?>" href="sobre-nos">Sobre Nós</a>
            </li>
            <li class="item-nav-li">
                <!-- <a class="<?php //if($URL_ATUAL == '/adotepet/transparencia/'){echo 'nav-active';};?>" href="transparencia">Transparência</a> -->
            </li>
            <!-- <li class="item-nav-li">
                <a class="<?php //if($URL_ATUAL == '/adotepet/blog/'){echo 'nav-active';};?>" href="blog">Blog</a>
            </li> -->
            <li class="como-ajudar-li"><a class="como-ajudar" href="como-ajudar">Como Ajudar ?</a></li>
            <li class="quero-adotar-li"><a class="quero-adotar" href="quero-adotar">Quero Adotar</a></li>
        </ul>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    <li class="logo-sidenav">
        <a href="<?php echo get_site_url(); ?>" class="brand-logo">
            <img class="logo" src="<?php echo $template_directory . 'logo.png' ?>" alt="">
        </a>
    </li>
    <li>&nbsp;</li>
    <li class="item-nav-li-m"><a href="sobre-nos">Sobre Nós</a></li>
    <!-- <li class="item-nav-li-m"><a href="transparencia">Transparência</a></li> -->
    <!-- <li class="item-nav-li-m"><a href="blog">Blog</a></li> -->
    <li class="como-ajudar-li-m"><a class="como-ajudar-m" href="como-ajudar">Como Ajudar ?</a></li>
    <li class="quero-adotar-li-m"><a class="quero-adotar-m" href="quero-adotar">Quero Adotar</a></li>
</ul>