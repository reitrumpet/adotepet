<?php
get_header();
$template_directory = get_template_directory_uri() . "/img/";
?>

<div class="container">
    <div class="center-align">
        <h3>Ajude-nos</h3>
        <h5>Saiba como contrinuar e ajudar na continuidade desse projeto</h5>
    </div>
    <div class="row">
        <div class="col s12 m4" style='background-image:url("<?php echo $template_directory; ?>bg-sobre-nos.png"); height: 500px; overflow: hidden; background-size: cover;'>
            &nbsp;
        </div>
        <div class="col s12 m8 center-align padTop6p">
            <div class="pix">
                <div class="fontsize25"> <b> Chaves PIX </b></div>
                <div class="fontsize20">Chave 1</div>
                <div class="fontsize20">Chave 2</div>
            </div>
            <div class="contas-bancos padTop20">
                <div class="fontsize25"><b>Contas Bancos</b></div>
                <div class="fontsize20">Banco 1</div>
                <div class="fontsize20">Banco 2</div>
            </div>
            <div class="Contatos padTop20">
            <div class="fontsize25"><b>Fale conosco !</b></div>
            <div>
            <a target="_blank" href="https://api.whatsapp.com/send?phone=+5582999097663&text=Mensagem%20enviada%20atrav%C3%A9s%20do%20AdotePet%20-%20Quero%20saber%20como%20ajudar%20" class="waves-effect waves-light btn">Fale conosco</a>
            </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>