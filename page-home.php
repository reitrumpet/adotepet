<?php get_header();

$template_directory = get_template_directory_uri() . "/img/";
?>
<?php
echo do_shortcode('[smartslider3 slider="2"]');
?>

<section class="container padding-top-15">
    <div class="row center-align">
        <div class="col s12 m6 section-adote ">

            “Ter um animal em sua vida te faz um humano melhor”
            <div class="row">
                <a href="quero-adotar" class="waves-effect waves-light btn">Adote</a>
            </div>
        </div>
        <div class="col s12 m6">
            <img src="<?php echo $template_directory; ?>img-pata.png" />
        </div>
    </div>
</section>
<section class="bg-azul">
    <div class=" container">
        <div class="row center-align section-animais-perdidos">
            <div class="titulo-animais-perdidos">
                Animais Perdidos
            </div>
            <div class="animais-perdidos col s12">
                <?php

                if (have_posts()) :
                    query_posts(array('category_name'  => 'animais-perdidos', 'posts_per_page' => 9));
                    while (have_posts()) : the_post();
                        $imagem = get_field('img');
                        if (!empty($imagem)) :
                ?>
                            <div class="col s12 m4">
                                <div class="nome-animal-perdido">
                                    <?php the_title(); ?>
                                </div>
                                <div class="img-animais-perdidos">
                                    <img class="img-servicos-home" src="<?php echo esc_url($imagem['url']); ?>" alt="<?php echo esc_attr($imagem['alt']); ?>" />
                                </div>
                                <?php
                                if (get_field('contato')) :
                                ?>
                                    <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php the_field('contato'); ?>&text=Mensagem%20enviada%20atrav%C3%A9s%20do%20AdotePet%20-%20Tenho%20informa%C3%A7%C3%B5es%20sobre%20o%20<?php the_title(); ?>%20" class="waves-effect waves-light btn btn-animal-perdido">Fale com o tutor</a>
                            </div>
                        <?php
                                endif;
                        ?>
            <?php
                        endif;
                    endwhile;
                endif;
            ?>

            </div>
        </div>
    </div>
</section>
<div class="parallax-container">
    <div class="parallax"><img src="<?php echo $template_directory; ?>parallax.png"></div>
</div>
<div style='background-image:url("<?php echo $template_directory; ?>bg-info.png"); height:200px;'>
    <div class="container">
        <div class="row">
            <div class="col s12 m12">
                <?php
                if (have_posts()) :
                    query_posts(array('category_name'  => 'reencontros', 'posts_per_page' => 1));
                    while (have_posts()) : the_post();
                ?>
                        <div class="col s12 m4 center-align">
                            <div class="col s12 numero-info">
                                <?php
                                if (get_field('numero')) :
                                    the_field('numero');
                                endif;
                                ?>
                            </div>
                            <div class="col s12 titulo-info">
                                <?php the_title(); ?>
                            </div>
                        </div>
                <?php
                    endwhile;
                endif;
                ?>

                <?php
                if (have_posts()) :
                    query_posts(array('category_name'  => 'adocoes', 'posts_per_page' => 1));
                    while (have_posts()) : the_post();
                ?>
                        <div class="col s12 m4 center-align">
                            <div class="col s12 numero-info">
                                <?php
                                if (get_field('numero')) :
                                    the_field('numero');
                                endif;
                                ?>
                            </div>
                            <div class="col s12 titulo-info">
                                <?php the_title(); ?>
                            </div>
                        </div>
                <?php
                    endwhile;
                endif;
                ?>

                <?php
                if (have_posts()) :
                    query_posts(array('category_name'  => 'parceiros', 'posts_per_page' => 1));
                    while (have_posts()) : the_post();
                ?>
                        <div class="col s12 m4 center-align">
                            <div class="col s12 numero-info">
                                <?php
                                if (get_field('numero')) :
                                    the_field('numero');
                                endif;
                                ?>
                            </div>
                            <div class="col s12 titulo-info">
                                <?php the_title(); ?>
                            </div>
                        </div>
                <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>