<?php
get_header();
$template_directory = get_template_directory_uri() . "/img/";
?>
<!-- <div class="parallax-container">
    <div class="parallax"><img src="<?php //echo $template_directory; 
                                    ?>parallax.png"></div>
</div> -->
<div class="container">
    <div class="row">
        <h3 class="center-align">ADOTE PET</h3>
        <h5 class="center-align">Seja a esperança na vida de um PET</h5>

        <div class="col s12 center-align">
            <?php

            if (have_posts()) :
                query_posts(array('category_name'  => 'para-adotar', 'posts_per_page' => 9));
                while (have_posts()) : the_post();
                    $imagem = get_field('imagem');
                    if (!empty($imagem)) :
            ?>
                        <div class="col s12 m4 center-align">
                            <div class="nome-animal-para-adotar">
                                <?php the_title(); ?>
                            </div>
                            <div class="img-animais-para-adotar">
                                <img class="img-servicos-home" src="<?php echo esc_url($imagem['url']); ?>" alt="<?php echo esc_attr($imagem['alt']); ?>" />
                            </div>
                            <div class="btn-quero-adotar">
                            <?php
                                if (get_field('numero')) :
                                ?>
                                    <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php the_field('numero'); ?>&text=Mensagem%20enviada%20atrav%C3%A9s%20do%20AdotePet%20-%20Quero%20adotar%20o%20<?php the_title(); ?>%20" class="waves-effect waves-light btn">Fale conosco</a>
                                <?php endif;?>
                            </div>
                        </div>

            <?php
                    endif;
                endwhile;
            endif;
            ?>
        </div>

    </div>
</div>

<?php get_footer(); ?>