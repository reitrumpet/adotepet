<?php get_header();
$template_directory = get_template_directory_uri() . "/img/";

?>

<div class="parallax-container">
    <div class="parallax"><img src="<?php echo $template_directory; ?>parallax.png"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col s12 m8">
            <?php
            if (have_posts()) :
                while (have_posts()) : the_post(); ?>
                    <h1 class="center-align"><?php the_title(); ?></h1>

                    <p> <?php the_content(); ?></p>
                    <div>
                        <h3 class="center-align">
                            <?php
                            if (get_field('titulo_1')) :
                                the_field('titulo_1');
                            endif;
                            ?>
                        </h3>
                        <p>
                            <?php
                            if (get_field('descricao_1')) :
                                the_field('descricao_1');
                            endif;
                            ?>
                        </p>
                        <h3 class="center-align">
                            <?php
                            if (get_field('titulo_2')) :
                                the_field('titulo_2');
                            endif;
                            ?>
                        </h3>
                        <p>
                            <?php
                            if (get_field('descricao_2')) :
                                the_field('descricao_2');
                            endif;
                            ?>
                        </p>
                        <h3 class="center-align">
                            <?php
                            if (get_field('titulo_3')) :
                                the_field('titulo_3');
                            endif;
                            ?>
                        </h3>
                        <p>
                            <?php
                            if (get_field('descricao_3')) :
                                the_field('descricao_3');
                            endif;
                            ?>
                        </p>
                    </div>
            <?php
                // the_content();
                endwhile;
            endif;
            ?>
        </div>
        <div class="col s12 m4" style='background-image:url("<?php echo $template_directory; ?>bg-sobre-nos.png"); height: 760px; overflow: hidden; background-size: cover;'>
            &nbsp;
        </div>
    </div>
</div>
<?php get_footer(); ?>