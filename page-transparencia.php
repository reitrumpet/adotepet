<?php get_header();
$template_directory = get_template_directory_uri() . "/img/";

?>

<div class="parallax-container">
    <div class="parallax"><img src="<?php echo $template_directory; ?>parallax.png"></div>
</div>
<div class="container">
    <div class="row">
        <?php
        if (have_posts()) :
            while (have_posts()) : the_post(); ?>
                <h1 class="center-align"><?php the_title(); ?></h1>
        <?php
                the_content();
            endwhile;
        endif;
        ?>
    </div>
</div>
<?php get_footer(); ?>